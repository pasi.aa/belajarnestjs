import { EntityRepository, Repository } from "typeorm";
import { User } from "../entity/user.entity";
import * as bcrypt from 'bcrypt';
import { CreateUserDto } from "../dto/create-user.dto";
import { ConflictException, InternalServerErrorException } from "@nestjs/common";

@EntityRepository(User)
export class UserRepository extends Repository<User> {
    async createUser(createUserDto: CreateUserDto): Promise<void> {
        const { name, email, password } = createUserDto;

        const user = this.create();
        user.name = name;
        user.email = email;
        user.salt = await bcrypt.genSalt();
        user.password = await bcrypt.hash(password, user.salt);

        try {
            await user.save();
        } catch (e) {
            if (e.code == 'ER_DUP_ENTRY') {
                throw new ConflictException(`Email ${email} already exist`);
            } else {
                throw new InternalServerErrorException(e);
            }
        }
    }
}